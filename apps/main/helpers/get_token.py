from django.urls import reverse
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient

from main.models import User

TEST_USER1 = 'test1@example.com'
CURRENT_PASSWORD = 'current_password_123'


def authentication(username, password):
    url = reverse('main_auth')
    data = {'username': username, 'password': password}

    client = APIClient()
    return client.post(url, data, format='json')


def create_user():
    user, _ = User.objects.get_or_create(username='test', password='test_password123')
    return user


def get_token():
    # Authentication
    # response = authentication(username, password)
    user = create_user()
    token, _ = Token.objects.get_or_create(user=user)
    return token.key
