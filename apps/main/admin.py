from django.contrib import admin

from main.models import Acceptor, AutoCompany, Buyer, District, Organization, Region, Selection, Station, Transport, \
    VehicleType


admin.site.register(Acceptor)
admin.site.register(AutoCompany)
admin.site.register(Buyer)
admin.site.register(District)
admin.site.register(Organization)
admin.site.register(Region)
admin.site.register(Selection)
admin.site.register(Station)
admin.site.register(Transport)
admin.site.register(VehicleType)
