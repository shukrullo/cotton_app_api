from django.db import models
from main.models.base_model import BaseModel


class Buyer(BaseModel):
    name = models.CharField(max_length=255)
    inn = models.BigIntegerField(null=True, blank=True)

    def __str__(self):
        return self.name
