from django.db import models
from main.models.base_model import BaseModel


class Transport(BaseModel):
    gos_number = models.CharField(max_length=255)
    driver = models.CharField(max_length=255)
    organization = models.ForeignKey('main.Organization', on_delete=models.CASCADE, related_name='transports',
                                     null=True)
    vehicle_type = models.ForeignKey('main.VehicleType', on_delete=models.CASCADE, related_name='transports')
    buyer = models.ForeignKey('main.Buyer', on_delete=models.CASCADE, related_name='transports', null=True, blank=True)
    auto_company = models.ForeignKey('main.AutoCompany', on_delete=models.PROTECT, related_name='transports')

    def __str__(self):
        return self.gos_number
