from django.db import models
from main.models.base_model import BaseModel


class AutoCompany(BaseModel):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name
