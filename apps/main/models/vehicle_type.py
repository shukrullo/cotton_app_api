from django.db import models
from main.models.base_model import BaseModel
from cotton.settings import AUTO_TYPES, TARIFF_TYPES


class VehicleType(BaseModel):
    name = models.CharField(max_length=200)
    auto_type = models.CharField(max_length=255, choices=AUTO_TYPES)
    truck_count = models.IntegerField()
    tariff_type = models.CharField(max_length=200, choices=TARIFF_TYPES)

    def __str__(self):
        return self.name
