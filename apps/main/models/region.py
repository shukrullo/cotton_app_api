from django.db import models
from main.models.base_model import BaseModel


class Region(BaseModel):
    code = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    ratio = models.DecimalField(max_digits=17, decimal_places=2, default=1)

    def __str__(self):
        return self.name
