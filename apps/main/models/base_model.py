from django.utils.datetime_safe import datetime
from django.db import models


def get_timestamp():
    return datetime.now().timestamp()


class QuerySet(models.QuerySet):
    def delete(self):
        self.update(is_delete=True)


class BaseManager(models.Manager):
    def get_queryset(self):
        return QuerySet(self.model).filter(is_delete=False)


class BaseModel(models.Model):
    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)
    tm = models.DecimalField(default=get_timestamp, decimal_places=9, max_digits=20)
    is_delete = models.BooleanField(default=False)
    one_c_key = models.CharField(max_length=255, null=True, blank=True)
    objects = BaseManager()

    def delete(self, using=None, keep_parents=False):
        self.is_delete = True
        self.save()

    class Meta:
        abstract = True
