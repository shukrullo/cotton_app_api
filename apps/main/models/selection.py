from django.db import models
from main.models.base_model import BaseModel
from cotton.settings import COTTON_TYPES


class Selection(BaseModel):
    code = models.CharField(max_length=100)
    name = models.CharField(max_length=255)
    correction = models.DecimalField(max_digits=6, decimal_places=2)
    cotton_type = models.CharField(max_length=255, choices=COTTON_TYPES)
    fiber_length = models.CharField(max_length=255)

    def __str__(self):
        return self.name
