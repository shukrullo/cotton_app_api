from django.db import models
from main.models.base_model import BaseModel


class Organization(BaseModel):
    code = models.CharField(max_length=255)
    inn = models.CharField(max_length=50)
    name = models.CharField(max_length=255)

    region = models.ForeignKey('main.Region', on_delete=models.CASCADE, related_name='orgs', null=True)
    district = models.ForeignKey('main.District', on_delete=models.CASCADE, related_name='orgs')

    def __str__(self):
        return self.name
