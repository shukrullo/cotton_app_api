from django.db import models
from main.models.base_model import BaseModel


class District(BaseModel):
    code = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    region = models.ForeignKey('main.Region', on_delete=models.CASCADE, related_name='districts')

    def __str__(self):
        return self.name
