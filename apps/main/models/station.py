from django.db import models
from main.models.base_model import BaseModel


class Station(BaseModel):
    code = models.CharField(max_length=255)
    org = models.ForeignKey('main.Organization', on_delete=models.CASCADE, related_name='stations')
    name = models.CharField(max_length=255)
    is_inner = models.SmallIntegerField(default=0)

    def __str__(self):
        return self.name
