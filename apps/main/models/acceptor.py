from main.models.base_model import BaseModel
from django.db import models


class Acceptor(BaseModel):
    login = models.CharField(max_length=255)
    password = models.CharField(max_length=255)
    fullname = models.CharField(max_length=255)
    full_role = models.CharField(max_length=255)
    short_role = models.CharField(max_length=255)

    station = models.ForeignKey('main.Station', on_delete=models.CASCADE, related_name='acceptors')

    def __str__(self):
        return self.fullname

    @property
    def org(self):
        return self.station.org
