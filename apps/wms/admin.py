from django.contrib import admin

from wms.models import BaleWeighing, Devices, BalePost, WmsOrder, OrderBaleWeighing, Lot, Client

admin.site.register(BalePost)
admin.site.register(BaleWeighing)
admin.site.register(Devices)
admin.site.register(WmsOrder)
admin.site.register(OrderBaleWeighing)
admin.site.register(Lot)
admin.site.register(Client)
