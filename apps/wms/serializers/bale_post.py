from rest_framework import serializers

from wms.models import BalePost
from wms.serializers.selection import SelectionSerializer


class BalePostSerializer(serializers.ModelSerializer):
    selection = SelectionSerializer()

    class Meta:
        model = BalePost
        fields = '__all__'


class BarcodeSerializer(serializers.Serializer):
    barcode = serializers.CharField()

