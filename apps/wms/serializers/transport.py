from main.models import Transport
from rest_framework import serializers


class TransportSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transport
        fields = '__all__'


class TransportSelectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transport
        fields = [
            'id',
            'gos_number'
        ]
