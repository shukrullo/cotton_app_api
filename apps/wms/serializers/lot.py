from rest_framework import serializers

from wms.models.lot import Lot


class LotSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lot
        fields = ('id', 'name', 'selection', 'cotton_sort', 'cotton_class', 'bale_count')
