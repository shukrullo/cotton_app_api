from rest_framework.serializers import ModelSerializer
from wms.models import WmsOrder, Client, BaleWeighing
from rest_framework import serializers

from wms.serializers.lot import LotSerializer
from drf_yasg.utils import swagger_serializer_method

from wms.serializers.transport import TransportSelectSerializer


class BaleWeighingSelectSerializer(serializers.ModelSerializer):
    lot = LotSerializer()

    class Meta:
        model = BaleWeighing
        fields = [
            'id',
            'barcode',
            'weight_gross',
            'weight_net',
            'weight_number',
            'is_lint',
            'lot',
            'is_accepted'
        ]


class OrderModelSerializer(ModelSerializer):
    class Meta:
        model = WmsOrder
        fields = [
            'id',
            'transport',
            'user',
            'type',
            'weight_count',
            'total_kg',
            'start_datetime',
            'end_datetime',
            'status'
        ]

        extra_kwargs = {
            'user': {'required': False},
            'transport': {'required': True}
        }

    def to_representation(self, instance):
        self.fields['status'] = serializers.CharField(source='get_status')
        self.fields['transport'] = TransportSelectSerializer()
        return super(OrderModelSerializer, self).to_representation(instance)


class OrderDetailSerializer(serializers.ModelSerializer):
    bale_weighing = serializers.SerializerMethodField()

    class Meta:
        model = WmsOrder
        fields = [
            'id',
            'status',
            'transport',
            'user',
            'type',
            'weight_count',
            'total_kg',
            'start_datetime',
            'end_datetime',
            'bale_weighing'
        ]

    @swagger_serializer_method(serializer_or_field=BaleWeighingSelectSerializer(many=True))
    def get_bale_weighing(self, obj):
        weighing = []
        objects = obj.order_bales.all()
        for item in objects:
            weighing.append(item.bale_weighing)
        return BaleWeighingSelectSerializer(weighing, many=True).data

    def to_representation(self, instance):
        self.fields['transport_gos_number'] = serializers.CharField(source='transport')
        self.fields['user_fullname'] = serializers.CharField(source='user')
        return super(OrderDetailSerializer, self).to_representation(instance)


class LotsCountSerializer(serializers.Serializer):
    count = serializers.IntegerField()
    name = serializers.CharField(source='bale_weighing__lot__name')


class LotGroupBySerializer(serializers.Serializer):
    transport = serializers.CharField()
    count = serializers.IntegerField()
    lots = LotsCountSerializer(many=True)


class ChangeStatusSerializer(serializers.Serializer):
    status = serializers.ChoiceField(WmsOrder.STATUS)


class BarcodeSelectSerializer(serializers.Serializer):
    barcode = serializers.CharField()

    def validate(self, attrs):
        barcode = attrs['barcode']

        barcodes = BaleWeighing.objects.filter(barcode=barcode)
        if barcodes.exists() is False:
            raise serializers.ValidationError(f'Не существует такой баркод {barcode}')
        attrs['barcode'] = barcodes.first()
        return attrs


class ClientOrderSerializer(serializers.Serializer):
    client = serializers.PrimaryKeyRelatedField(queryset=Client.objects.all())
    barcodes = BarcodeSelectSerializer(many=True)


class OrderTypeFilterSerializer(serializers.Serializer):
    FILTER_FIELDS = (('unload', 'unload'), ('shipment', 'shipment'), ('client', 'client'))
    type_filter = serializers.ChoiceField(FILTER_FIELDS, required=False)
