from rest_framework import serializers
from wms.models import BaleWeighing, OrderBaleWeighing, Lot
from wms.serializers.lot import LotSerializer


class BaleWeighingSerializer(serializers.ModelSerializer):
    lot = LotSerializer()

    class Meta:
        model = BaleWeighing
        fields = [
            'id',
            'organization',
            'device',
            'barcode',
            'weight_gross',
            'weight_net',
            'weight_number',
            'is_lint',
            'lot',
            'is_accepted'
        ]


class OrderBaleWeighingSerializer(serializers.ModelSerializer):
    barcode = serializers.CharField(write_only=True)

    class Meta:
        model = OrderBaleWeighing
        fields = ['order', 'barcode']

    def validate(self, attrs):
        bale_weighing = BaleWeighing.objects.filter(barcode=attrs['barcode'])
        if bale_weighing.exists() is False:
            raise serializers.ValidationError(f'штрих-код не найден {attrs["barcode"]}')
        attrs.pop('barcode')
        attrs['bale_weighing'] = bale_weighing.first()
        check = self.Meta.model.objects.filter(bale_weighing=attrs['bale_weighing']).exists()
        if check:
            raise serializers.ValidationError(f"{attrs['bale_weighing'].barcode} этот баркод ужe существует")
        return attrs


class LotValidationSerializer(serializers.Serializer):
    lot = serializers.PrimaryKeyRelatedField(queryset=Lot.objects.all())
