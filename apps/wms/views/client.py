from rest_framework import viewsets

from wms.models.client import Client
from wms.serializers.client import ClientSerializer


class ClientViewSet(viewsets.ModelViewSet):
    model = Client
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
    filterset_fields = ('name', 'inn', )
