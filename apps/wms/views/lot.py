from rest_framework import viewsets

from wms.models.lot import Lot
from wms.serializers.lot import LotSerializer


class LotViewSet(viewsets.ModelViewSet):
    model = Lot
    queryset = Lot.objects.all()
    serializer_class = LotSerializer
