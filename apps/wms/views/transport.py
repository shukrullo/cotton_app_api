from drf_yasg.utils import swagger_auto_schema
from wms.serializers.transport import TransportSerializer
from main.models.transport import Transport
from rest_framework import viewsets


class TransportViewSet(viewsets.ModelViewSet):
    queryset = Transport.objects.all()
    model = Transport
    serializer_class = TransportSerializer
    order_by = ('gos_number',)
    search_fields = ('gos_number',)
