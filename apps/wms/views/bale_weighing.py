from drf_yasg.utils import swagger_auto_schema
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.viewsets import ModelViewSet
from wms.serializers.bale_weighing import BaleWeighingSerializer, OrderBaleWeighingSerializer, LotValidationSerializer
from wms.models import BaleWeighing, OrderBaleWeighing


class BaleWeightViewSet(ModelViewSet):
    queryset = BaleWeighing.objects.all()
    serializer_class = BaleWeighingSerializer

    @swagger_auto_schema(tags=['wms'], request_body=LotValidationSerializer)
    @action(methods=['POST'], detail=True)
    def set_lot(self, request, pk):
        bale_detail = self.get_object()
        serializer = LotValidationSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        lot = serializer.validated_data['lot']
        bale_weight_count = BaleWeighing.objects.filter(lot_id=lot).count()
        lot_limit = 220

        if bale_weight_count == lot_limit:
            raise ValidationError(f'Этот лот {lot.name} уже имеет 220 кипов')
        bale_detail.is_accepted = True
        bale_detail.lot = lot
        bale_detail.save()
        return Response(dict(detail='ok'))

    @action(methods=['GET'], detail=True)
    def accepted(self, request, pk):
        bale_detail = self.get_object()
        bale_detail.is_accepted = True
        bale_detail.save()
        return Response(dict(detail='ok'))


class OrderBaleWeightViewSet(ModelViewSet):
    queryset = OrderBaleWeighing.objects.all()
    serializer_class = OrderBaleWeighingSerializer
    filter_fields = ('order_id',)
