from drf_yasg.utils import swagger_auto_schema
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from wms.serializers.bale_post import BarcodeSerializer

from wms.models import BalePost
from wms.serializers.bale_post import BalePostSerializer


class BalePostViewSet(viewsets.ModelViewSet):

    model = BalePost
    queryset = BalePost.objects.all()
    serializer_class = BalePostSerializer

    @swagger_auto_schema(responses={200: BalePostSerializer}, tags=['wms'], query_serializer=BarcodeSerializer)
    @action(methods=['GET'], detail=False)
    def barcode_filter(self, request):
        serializer = BarcodeSerializer(data=self.request.GET, many=False)
        serializer.is_valid(raise_exception=True)
        barcode = serializer.validated_data['barcode']
        get_barcode = self.queryset.filter(barcode=barcode).first()
        barcode_response = BalePostSerializer(get_barcode).data
        return Response(barcode_response)
