from django.db.models import Count
from django.utils.datetime_safe import datetime
from drf_yasg.utils import swagger_auto_schema
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from wms.models import BaleWeighing, OrderBaleWeighing
from wms.models.order import WmsOrder
from wms.serializers.order import OrderModelSerializer, OrderDetailSerializer, LotGroupBySerializer, \
    ChangeStatusSerializer, ClientOrderSerializer, OrderTypeFilterSerializer


class OrderModelViewSet(ModelViewSet):
    queryset = WmsOrder.objects.all().order_by('status', 'id')
    serializer_class = OrderModelSerializer
    FILTERS = {
        'unload': [WmsOrder.NEW, WmsOrder.PROCESSING],
        'shipment': [WmsOrder.PROCESSING, WmsOrder.LOADING, WmsOrder.FINISH],
        'client': [WmsOrder.NEW, WmsOrder.FINISH]
    }

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def get_queryset(self):
        if getattr(self, 'swagger_fake_view', False) is False:
            queryset = self.queryset.filter(user=self.request.user)
            type_filter = OrderTypeFilterSerializer(data=self.request.GET)
            type_filter.is_valid(raise_exception=True)
            type_filter = type_filter.validated_data.get('type_filter')
            if type_filter:
                filters = self.FILTERS[type_filter]
                queryset = self.queryset.filter(status__in=filters)
                if type_filter == 'client':
                    queryset = queryset.filter(type=WmsOrder.CLIENT)
            return queryset

    def retrieve(self, request, *args, **kwargs):
        self.serializer_class = OrderDetailSerializer
        return super(OrderModelViewSet, self).retrieve(request, *args, **kwargs)

    @swagger_auto_schema(method='POST', request_body=ChangeStatusSerializer, responses={200: 'ok'})
    @action(methods=['POST'], detail=True)
    def change_status(self, request, pk):
        order = self.get_object()
        serializer = ChangeStatusSerializer(data=request.data, many=False)
        serializer.is_valid(raise_exception=True)
        status = serializer.validated_data['status']
        if status == WmsOrder.PROCESSING:
            order.end_datetime = datetime.now()
        if status == WmsOrder.FINISH and order.end_datetime is None:
            order.end_datetime = datetime.now()
            order.save()
        order.status = status
        order.save()
        return Response({"detail": "ok"})

    @swagger_auto_schema(responses={200: LotGroupBySerializer}, method='GET')
    @action(methods=['GET'], detail=True)
    def lot_detail(self, request, pk):
        data = dict()
        order = self.get_object()
        weights = order.order_bales.filter(bale_weighing__lot__isnull=False)
        bale_weight_count = weights.count()
        group_by_lot = weights.values('bale_weighing__lot__name').annotate(count=Count('*')).order_by()
        data.update({
            'transport': order.transport.gos_number,
            'count': bale_weight_count,
            'lots': group_by_lot
        })

        return Response(LotGroupBySerializer(data).data)

    @swagger_auto_schema(methods=['POST'], request_body=ClientOrderSerializer)
    @action(methods=['POST'], detail=False)
    def create_order_for_client(self, request):
        serializer = ClientOrderSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        client = serializer.validated_data['client']
        barcodes = serializer.validated_data['barcodes']
        order = WmsOrder.objects.create(client=client, type=WmsOrder.CLIENT, status=WmsOrder.NEW, user=request.user)

        for barcode in barcodes:
            OrderBaleWeighing.objects.create(order=order, bale_weighing=barcode['barcode'])

        return Response({"detail": "ok"})
