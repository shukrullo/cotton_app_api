from django.urls import path, include

from wms.views.bale_post import BalePostViewSet
from wms.views.client import ClientViewSet
from wms.views.lot import LotViewSet
from wms.views.transport import TransportViewSet
from wms.views.order import OrderModelViewSet
from wms.views.bale_weighing import BaleWeightViewSet, OrderBaleWeightViewSet
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register('order', OrderModelViewSet, 'order')
router.register('bale_weight', BaleWeightViewSet, 'bale_weight')
router.register('order_bale_weight', OrderBaleWeightViewSet, 'order_bale_weight')
router.register('bale_post', BalePostViewSet, 'bale_post')
router.register('lot', LotViewSet, 'lot')
router.register('transport', TransportViewSet, 'transport')
router.register('client', ClientViewSet, 'client')


urlpatterns = [
    path('', include(router.urls))
]
