import datetime
from django.urls import reverse
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase

from main.helpers.get_token import get_token
from wms.models import Lot
from wms.serializers.lot import LotSerializer


class GetLotTest(APITestCase):
    def setUp(self):
        Lot.objects.create(
            name='Lot1',
        ),
        Lot.objects.create(
            name='Lot2',
        )

        self.client.credentials(HTTP_AUTHORIZATION='Token ' + get_token())

    list_url = 'lot-list'
    detail_url = 'lot-detail'

    def test_list(self):
        # get API responses
        response = self.client.get(reverse(self.list_url))
        self.assertEqual(response.data['count'], 2)
        self.assertEqual(response.data['results'][0]['name'], 'Lot1')
        self.assertEqual(response.data['results'][1]['name'], 'Lot2')
        # get data from db
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update(self):
        url = reverse(self.detail_url, kwargs={'pk': 1})
        data = {'name': 'New lot name'}
        response = self.client.put(url, data, format='json')


