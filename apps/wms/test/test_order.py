

import datetime
from django.urls import reverse
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase

from main.helpers.get_token import get_token
from main.models import Transport
from wms.models import WmsOrder
from wms.serializers.lot import LotSerializer


class WmsOrderTest(APITestCase):
    def setUp(self):
        fixtures = [

        ]

        self.client.credentials(HTTP_AUTHORIZATION='Token ' + get_token())

    list_url = 'order-list'
    detail_url = 'order-detail'

    def test_create(self):

        WmsOrder.objects.create(
            transport_id=2,
            type='client',
            user_id=1,
        ),
        WmsOrder.objects.create(
            transport_id=2,
            type='stock',
            user_id=1,
        )

        response = self.client.post(reverse(self.list_url))
        self.assertEqual(response.data['results'][0]['transport_id'], 1)
        self.assertEqual(response.data['results'][1]['transport_id'], 1)
        self.assertEqual(response.status_code, status.HTTP_200_OK)