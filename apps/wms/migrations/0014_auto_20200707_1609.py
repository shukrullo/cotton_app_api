# Generated by Django 3.0.8 on 2020-07-07 11:09

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('wms', '0013_auto_20200707_1607'),
    ]

    operations = [
        migrations.RenameField(
            model_name='lot',
            old_name='otton_sort',
            new_name='cotton_sort',
        ),
    ]
