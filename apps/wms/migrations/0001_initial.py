# Generated by Django 3.0.8 on 2020-07-01 11:14

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import main.models.base_model


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='BaleWeighing',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('tm', models.DecimalField(decimal_places=9, default=main.models.base_model.get_timestamp, max_digits=20)),
                ('is_delete', models.BooleanField(default=False)),
                ('one_c_key', models.CharField(max_length=255, null=True)),
                ('barcode', models.CharField(max_length=255)),
                ('weight_gross', models.DecimalField(decimal_places=1, max_digits=20)),
                ('weight_net', models.DecimalField(decimal_places=1, max_digits=20)),
                ('weight_number', models.CharField(max_length=255)),
                ('is_lint', models.BooleanField(default=False)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='WmsOrder',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('tm', models.DecimalField(decimal_places=9, default=main.models.base_model.get_timestamp, max_digits=20)),
                ('is_delete', models.BooleanField(default=False)),
                ('one_c_key', models.CharField(max_length=255, null=True)),
                ('status', models.CharField(choices=[('new', 'new'), ('processing', 'processing'), ('finish', 'finish')], max_length=25)),
                ('transport', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.Transport')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='OrderBaleWeighing',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('tm', models.DecimalField(decimal_places=9, default=main.models.base_model.get_timestamp, max_digits=20)),
                ('is_delete', models.BooleanField(default=False)),
                ('one_c_key', models.CharField(max_length=255, null=True)),
                ('bale_weighing', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='wms.BaleWeighing')),
                ('order', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='order_bales', to='wms.WmsOrder')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Devices',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('modified_date', models.DateTimeField(auto_now=True)),
                ('tm', models.DecimalField(decimal_places=9, default=main.models.base_model.get_timestamp, max_digits=20)),
                ('is_delete', models.BooleanField(default=False)),
                ('one_c_key', models.CharField(max_length=255, null=True)),
                ('name', models.CharField(max_length=255)),
                ('line_number', models.CharField(max_length=255)),
                ('password', models.CharField(max_length=255)),
                ('started_weight_number', models.IntegerField(null=True)),
                ('started_lint_number', models.IntegerField(null=True)),
                ('organization', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.Organization')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='baleweighing',
            name='device',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='wms.Devices'),
        ),
        migrations.AddField(
            model_name='baleweighing',
            name='organization',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.Organization'),
        ),
    ]
