# Generated by Django 3.0.8 on 2020-07-03 12:45

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('wms', '0007_auto_20200703_1607'),
    ]

    operations = [
        migrations.AddField(
            model_name='wmsorder',
            name='type',
            field=models.CharField(choices=[('stock', 'stock'), ('client', 'client')], default=django.utils.timezone.now, max_length=50),
            preserve_default=False,
        ),
    ]
