from main.models.base_model import BaseModel
from django.db import models


class OrderBaleWeighing(BaseModel):
    bale_weighing = models.ForeignKey('wms.BaleWeighing', on_delete=models.CASCADE)
    order = models.ForeignKey('wms.WmsOrder', on_delete=models.CASCADE, related_name='order_bales')

    def __str__(self):
        return f"{self.pk}"
