from django.db import models

from main.models.base_model import BaseModel
from cotton.settings import COTTON_SORTS, COTTON_CLASSES


class Lot(BaseModel):
    name = models.CharField(max_length=255)
    selection = models.ForeignKey('main.Selection', on_delete=models.CASCADE, null=True)
    cotton_sort = models.CharField(choices=COTTON_SORTS, max_length=255, null=True)
    cotton_class = models.CharField(choices=COTTON_CLASSES, max_length=255, null=True)

    def __str__(self):
        return self.name

    @property
    def bale_count(self) -> int:
        return self.weighings.all().count()
