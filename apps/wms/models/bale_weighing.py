from django.db import models
from django.db.models import CASCADE

from main.models.base_model import BaseModel


class BaleWeighing(BaseModel):
    organization = models.ForeignKey('main.Organization', on_delete=models.CASCADE)
    device = models.ForeignKey('wms.Devices', on_delete=models.CASCADE)
    barcode = models.CharField(max_length=255)
    weight_gross = models.DecimalField(max_digits=20, decimal_places=1)
    weight_net = models.DecimalField(max_digits=20, decimal_places=1)
    weight_number = models.CharField(max_length=255)
    is_lint = models.BooleanField(default=False)
    lot = models.ForeignKey('wms.Lot', CASCADE, null=True, related_name='weighings')

    is_accepted = models.BooleanField(default=False)

    def __str__(self):
        return self.barcode
