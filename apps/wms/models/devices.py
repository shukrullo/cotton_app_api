from main.models.base_model import BaseModel
from django.db import models


class Devices(BaseModel):
    organization = models.ForeignKey('main.Organization', on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    line_number = models.CharField(max_length=255)
    password = models.CharField(max_length=255)
    started_weight_number = models.IntegerField(null=True)
    started_lint_number = models.IntegerField(null=True)

    def __str__(self):
        return self.name
