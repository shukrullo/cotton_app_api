from main.models.base_model import BaseModel
from django.db import models
from cotton.settings import COTTON_SORTS, COTTON_CLASSES


class BalePost(BaseModel):
    selection = models.ForeignKey('main.Selection', on_delete=models.CASCADE)
    cotton_sort = models.CharField(choices=COTTON_SORTS, max_length=255)
    cotton_class = models.CharField(choices=COTTON_CLASSES, max_length=255)
    barcode = models.CharField(max_length=255)

    def __str__(self):
        return self.barcode
