from django.db import models

from main.models.base_model import BaseModel


class Client(BaseModel):
    name = models.CharField(max_length=255)
    inn = models.CharField(max_length=255)

    def __str__(self):
        return self.name
