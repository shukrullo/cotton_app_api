from django.db import models
from django.db.models import CASCADE

from main.models.base_model import BaseModel


class WmsOrder(BaseModel):
    NEW = 1
    PROCESSING = 2
    LOADING = 3
    FINISH = 4

    STATUS = (
        (NEW, 'new'),
        (PROCESSING, 'processing'),
        (LOADING, 'loading'),
        (FINISH, 'finish')
    )

    STOCK = 'stock'
    CLIENT = 'client'

    ORDER_TYPE = (
        (STOCK, 'stock'),
        (CLIENT, 'client'),
    )

    status = models.CharField(choices=STATUS, max_length=25, default=NEW)
    start_datetime = models.DateTimeField(auto_now_add=True, null=True)
    end_datetime = models.DateTimeField(null=True)
    type = models.CharField(max_length=50, choices=ORDER_TYPE)

    client = models.ForeignKey('wms.Client', CASCADE, null=True)
    user = models.ForeignKey('main.User', on_delete=models.CASCADE)
    transport = models.ForeignKey('main.Transport', on_delete=models.CASCADE, null=True)

    @property
    def weight_count(self) -> int:
        return self.order_bales.all().count()

    @property
    def total_kg(self) -> int:
        return self.order_bales.all().aggregate(total_kg=models.Sum('bale_weighing__weight_net')).get('total_kg', 0) or 0

    @property
    def get_status(self):
        return dict(self.STATUS).get(int(self.status))

    def __str__(self):
        return f"{self.pk}"
