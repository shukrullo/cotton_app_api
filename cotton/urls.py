from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include

from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from main.authtoken import obtain_auth_token

schema_view = get_schema_view(
    openapi.Info(
        title="Cotton API",
        default_version='v1',
        description="WMS && ONE_C && ECAS && MONITORING ",
        contact=openapi.Contact(email="begymrx@gmail.com"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    url(r'^swagger/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    url(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    url(r'^api-token-auth/', obtain_auth_token, name='main_auth'),
    path('ecas/v1/', include('ecas.urls')),
    path('one_c/v1/', include('one_c.urls')),
    path('wms/v1/', include('wms.urls')),
    path('monitoring/v1/', include('monitoring.urls')),
]
